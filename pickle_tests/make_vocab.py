#!/usr/bin/env python3

import pickle
from build_vocab import Vocabulary


with open('vocab_yesterday.pkl', 'rb') as f:
    res = pickle.load(f)
print(res)

with open('vocab_today3a.pkl', 'wb') as fw:
    pickle.dump(res, fw, -1)

