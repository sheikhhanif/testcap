## Image-Captioning by Sheikh Hanif ( Flask web app + Api documentaion with Swagger )

### Usage 
To run this project  successfully in your local machine, you need to install all the necessary packages. Refer to the 'requirements.txt'. 

- pip install -r requirements.txt

- python app.py

The flask web-app will appear at this location: http://127.0.0.1:5000/
### Try live (deployed)
- Visit: http://hanif-img-caption.datamela.com/

You may upload an image and get caption as it's shown below
### Web UI with flask and REST

![demo-web-ui](https://bitbucket.org/joeyzbb/sheikh_hanif/raw/b588313c4307c01c572eebbb5507c9d619c26aa5/demo/web-ui.png)

### Swagger UI
You can test the API by using Swagger UI. Swagger is located at this location: http://hanif-img-caption.datamela.com/swagger/

A demo of swagger UI has attached below

![demo-web-ui](https://bitbucket.org/joeyzbb/sheikh_hanif/raw/b588313c4307c01c572eebbb5507c9d619c26aa5/demo/swagger-ui.png)

### API defination
You may access 'json' formatted swagger api specification from this link: http://hanif-img-caption.datamela.com/apispec_1.json
